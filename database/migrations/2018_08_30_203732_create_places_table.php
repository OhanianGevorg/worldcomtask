<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string("place_name");
            $table->unsignedInteger("country_code_id")->index();
            $table->double("latitude");
            $table->double("longitude");
            $table->string("state")->nullable();
            $table->string("state_abbreviation",100)->nullable();
            $table->timestamps();

            $table->foreign('country_code_id')
                ->references('id')
                ->on('country_codes')
            ->onUpdate('no action')
            ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
