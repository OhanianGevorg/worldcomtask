<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetPlacesRequest;
use App\Model\CountryCode;
use App\Services\GuzzleHttpService;
use App\Support\CountriesCollection;
use App\Support\DataMakers\CountryCodeDataMaker;
use App\Support\DataMakers\PlacesDataMaker;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\DB;

class CountryZipCodeController extends Controller
{
    /**
     * @var GuzzleHttpService
     */
    protected $guzzleService;

    /**
     * CountryZipCodeController constructor.
     * @param GuzzleHttpService $guzzleHttpService
     */
    public function __construct(GuzzleHttpService $guzzleHttpService)
    {
        $this->guzzleService = $guzzleHttpService;
    }

    /**
     * get zip code view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $countries = app()->make(CountriesCollection::class);

        return view("worldcom", compact("countries"));
    }

    /**
     * get places
     *
     * @param GetPlacesRequest $request
     * @param CountryCodeDataMaker $maker
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlaces(GetPlacesRequest $request, CountryCodeDataMaker $maker)
    {

        $countryCode = CountryCode::findBy([
            'zip_code' => $request->zip_code
        ])->first();


        if (!is_null($countryCode)) {

            return response()
                ->json([
                    'places' => $countryCode->places,
                    'zip_code' => $countryCode->zip_code
                ], 200);
        }

        try {
            $response = $this->guzzleService
                ->get("{$request->abbreviation}/{$request->zip_code}")
                ->getJsonEncoded();

            DB::beginTransaction();
            /**
             * @var $model CountryCode
             */
            $model = $maker->makeModel($response);

            $model->save();

            $model->places()->saveMany(PlacesDataMaker::collection($response->places));

            DB::commit();

            return response()
                ->json([
                    'places' => $model->places,
                    'zip_code' => $model->zip_code
                ], 201);

        } catch (BadResponseException $e) {

            $res = $e->getResponse();

            return $this->getErrorMessage(
                $res->getStatusCode(),
                "Places not loaded. Reason: " . $res->getReasonPhrase()
            );

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->getErrorMessage($e->getCode()

            );
        }

    }

    /**
     * format error output
     *
     * @param int $status
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getErrorMessage(int $status, $message = '')
    {
        $messageLength = strlen($message);

        switch ($status) {
            case 404:
                return response()
                    ->json([
                        'message' => $messageLength !== 0 ? $message : "Not Found"
                    ], $status);
            case 500:
                return response()
                    ->json([
                        'message' => $messageLength !== 0 ? $message : "Server can not handle request.Try again later"
                    ], $status);
            default:
                return response()
                    ->json([
                        'message' => $messageLength !== 0 ? $message : "Unhandled server error.Try again later"
                    ], 500);
        }
    }
}
