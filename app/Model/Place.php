<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'place_name',
        'country_code_id',
        'latitude',
        'longitude',
        'state',
        'state_abbreviation'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'country_code_id' => 'integer',
        'latitude' => 'double',
        'longitude' => 'double',
    ];

    public function countryCode()
    {
        return $this->belongsTo(CountryCode::class, "country_code_id", "id");
    }
}
