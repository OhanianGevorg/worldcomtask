<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CountryCode extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'country_codes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'zip_code',
        'abbreviation',
        'country'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function places()
    {
        return $this->hasMany(Place::class, "country_code_id", "id");
    }

    /**
     * get instance filtered by property
     *
     * @param Builder $query
     * @param array $fields
     * @return mixed
     */
    public function scopeFindBy(Builder $query, array $fields)
    {
        $whereClause = [];

        foreach ($fields as $key => $field) {
            $whereClause[] = [$key, '=', $field];
        }
        return $query
            ->where($whereClause);
    }
}
