<?php

namespace App\Services;


use GuzzleHttp\Client;

/**
 * @desc Sample GuzzleHttp wrapper
 *
 * Class GuzzleHttpService
 * @package App\Services
 */
class GuzzleHttpService
{
    /**
     * @var Client
     */
    protected $client;


    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $response;

    /**
     * GuzzleHttpService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * for not authenticated requests
     *
     * @param string $url
     * @param array $options
     * @return $this
     */
    public function get(string $url, array $options = [])
    {
        $this->response = $this->client->get($url, $options);

        return $this;
    }

    /**
     * get response
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * get response content
     *
     * @return \Psr\Http\Message\ResponseInterface|string
     */
    public function getContent()
    {
        if (is_null($this->response)) {
            return $this->response;
        }

        return $this->response->getBody()
            ->getContents();
    }

    /**
     * get json decoded content
     *
     * @param bool $isAssoc
     * @return object|array
     */
    public function getJsonEncoded($isAssoc = false)
    {
        return json_decode($this->getContent(), $isAssoc);
    }
}