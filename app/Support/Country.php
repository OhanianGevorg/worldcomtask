<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/1/18
 * Time: 2:12 PM
 */

namespace App\Support;

/**
 * wrapper for countries
 *
 * Class Country
 * @package App\Support
 */
class Country
{
    /**
     * @var String
     */
    protected $country = '';

    /**
     * @var String
     */
    protected $code = '';

    /**
     * Country constructor.
     * @param $item
     */
    public function __construct($item)
    {
        $this->country = $item->name;
        $this->code = $item->code;
    }

    /**
     * get country flag
     *
     * @param string $path
     * @param string $fileName
     * @return string
     */
    public function getFlag($path = 'img/country-flags/', $fileName = '')
    {
        if (strlen($fileName)) {
            $path .= $fileName;
        } else {
            $path .= strtolower($this->code) . '.png';
        }

        return asset("/{$path}");
    }

    /**
     * get short code
     *
     * @return String
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * get country name
     *
     * @return String
     */
    public function getCountry()
    {
        return $this->country;
    }
}