<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/1/18
 * Time: 4:20 AM
 */

namespace App\Support\DataMakers;


use App\Model\Place;
use Illuminate\Database\Eloquent\Model;

class PlacesDataMaker extends AbstractDataMaker
{
    public function makeModel($data): Model
    {
        return new Place([
            'place_name' => $data->{"place name"},
            'latitude' => $data->latitude,
            'longitude' => $data->longitude,
            'state' => $data->state,
            'state_abbreviation' => $data->{"state abbreviation"}
        ]);
    }
}