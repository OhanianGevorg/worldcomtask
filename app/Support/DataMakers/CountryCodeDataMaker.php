<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/1/18
 * Time: 4:18 AM
 */

namespace App\Support\DataMakers;


use App\Model\CountryCode;
use Illuminate\Database\Eloquent\Model;

class CountryCodeDataMaker extends AbstractDataMaker
{
    public function makeModel($data): Model
    {
       return new CountryCode([
            'zip_code' => $data->{"post code"},
            'abbreviation' => $data->{"country abbreviation"},
            'country' => $data->country,
        ]);
    }
}