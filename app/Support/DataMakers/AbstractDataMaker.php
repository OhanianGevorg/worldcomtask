<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/1/18
 * Time: 3:44 AM
 */

namespace App\Support\DataMakers;


use Illuminate\Database\Eloquent\Model;

abstract class AbstractDataMaker
{

    /**
     * @param $data
     * @return array
     */
    public static function collection($data)
    {

        if (!is_array($data) && !$data instanceof \Traversable) {
            throw new \InvalidArgumentException("data must be array or implement Traversable", 100);
        }

        $dataArr = [];

        foreach ($data as $datum) {
            $dataArr[] = (new static())->makeModel($datum);
        }

        return $dataArr;
    }

    /**
     * @param $data
     * @return Model
     */
    public abstract function makeModel($data): Model;

}