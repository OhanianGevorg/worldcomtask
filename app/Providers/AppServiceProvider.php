<?php

namespace App\Providers;

use App\Services\GuzzleHttpService;
use App\Support\CountriesCollection;
use App\Support\Country;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Guzzle Service
         */
        $this->app->bind(GuzzleHttpService::class, function () {

            return new GuzzleHttpService(new Client(['base_uri' => 'http://api.zippopotam.us']));
        });

        /**
         * countries json
         */
        $this->app->singleton('countries', function () {
            $json = file_get_contents(app_path('countries.json'));
            return json_decode($json);
        });

        /**
         * Countries collection with Country transformed items
         */
        $this->app->bind(CountriesCollection::class, function ($app) {
            return (new CountriesCollection($app->make('countries')))->transform(function ($item) {
                return new Country($item);
            });
        });
    }
}
