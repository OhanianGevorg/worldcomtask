<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>Worldcom</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/bootstrap/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/bootstrap/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/custom.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div class="position-ref full-height">
    <div class="row flex-column align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="form-wrapper m-card">
                <div class="m-card-header">
                    <h6>Country form</h6>
                </div>
                <div class="m-card-content">
                    <form class="form-inline" data-url="/places">

                        <div class="col-md-5">
                            <select class="form-control selectpicker  country-select" data-live-search="true"
                                    name="abbreviation">
                                <option value="" class="select-placeholder" selected disabled>Please select</option>
                                @foreach($countries as $key=>$country)
                                    <option data-content="<span class='country-list-item'><img width='22' alt='flag' src='{{$country->getFlag()}}'/>{{$country->getCountry()}}</span>"
                                            value="{{$country->getCode()}}"></option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-prepend">
                                    <div class="input-group-text "><img width="20"
                                                                        src="{{asset('img/zip-code-50.png')}}"
                                                                        alt="zip"></div>
                                </div>
                                <input type="text" name="zip_code" id="zip-code" placeholder="Type zip code"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary  get-places">Get places</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <div class="m-card margin-top-50">
                <div class="m-card-header">
                    <h6>Places</h6>
                </div>
                <div class="m-card-content">
                    <table class="table table-striped" id="place-results">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Latitude</th>
                            <th scope="col">Longitude</th>
                            <th scope="col">State</th>
                            <th scope="col">State abbreviation</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($countries as $key=>$country)--}}

                        {{--<tr>--}}
                        {{--<th scope="row">{{++$key}}</th>--}}
                        {{--<td>--}}
                        {{--<span class="country-list-item">--}}
                        {{--<img width='22' alt='flag' src='{{$country->getFlag()}}'/>--}}
                        {{--{{$country->getCountry()}}</span>--}}
                        {{--</td>--}}
                        {{--<td>zip code</td>--}}
                        {{--</tr>--}}
                        {{--@endforeach--}}

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="content">

    </div>
</div>
</body>
<script src="{{asset("/js/jquery/jquery.min.js")}}"></script>
<script src="{{asset("/js/bootstrap/popper.min.js")}}"></script>
<script src="{{asset("/js/bootstrap/bootstrap.min.js")}}"></script>
<script src="{{asset("/js/bootstrap/bootstrap-select.min.js")}}"></script>
<script src="{{asset("/js/bootstrap/bootstrap-notify.min.js")}}"></script>
<script src="{{asset("/js/validation_notify.js")}}"></script>
<script src="{{asset("/js/zip-code-page.js")}}"></script>
</html>
