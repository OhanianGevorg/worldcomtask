$(function () {
    var notify = ValidationNotify.notify,
        ajaxHeaders = {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            'Accept': 'application/json'
        },
        makeTable = function (data) {
            var tr = "";

            $.each(data, function (index, item) {
                tr += '<tr>' +
                    '<td>' + item.place_name + '</td>' +
                    '<td>' + item.latitude + '</td>' +
                    '<td>' + item.longitude + '</td>' +
                    '<td>' + (item.state?item.state:"-") + '</td>' +
                    '<td>' + (item.state_abbreviation?item.state_abbreviation:"-") + '</td>' +
                    '</tr>';
            });
            return tr;
        };

    $(".dropdown-item").on("click", function () {
        var _this = $(this),
            dropDownHeader = $("#country-dropdown");
        dropDownHeader.html(_this.html());

    });

    $(".get-places").on("click", function (e) {
        e.preventDefault();
        var _this = $(this),
            form = _this.closest('form'),
            url = form.attr('data-url'),
            data = form.serialize();

        $.ajax({
            headers: ajaxHeaders,
            method: "POST",
            data: data,
            url: url,
            success: function (res) {
                var table = $("#place-results");

                table.find("tbody").html(makeTable(res.places));
            },
            error: function (xhr) {
                notify(xhr.status, xhr.responseJSON);
            }
        })
    })
});