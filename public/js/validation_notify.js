var ValidationNotify = (function () {

        var notifyParams = {

                200: {
                    icon: 'fa fa-check',
                    type: 'success'
                },
                404: {
                    icon: 'fa fa-exclamation',
                    type: 'danger'
                },
                302: {
                    icon: 'fa fa-exclamation-triangle',
                    type: 'warning'
                },
                403: {
                    icon: 'fa fa-exclamation',
                    type: 'danger'
                },
                500: {
                    icon: 'fa fa-exclamation',
                    type: 'danger'
                },
                419: {
                    icon: 'fa fa-exclamation',
                    type: 'danger'
                },
                422: {
                    icon: 'fa fa-exclamation',
                    type: 'danger'
                },
                204: {
                    icon: 'fa fa-exclamation-triangle',
                    type: 'info'
                },
                202: {
                    icon: 'fa fa-exclamation-triangle',
                    type: 'info'
                }

            },

            clearErrors = function (inputs) {
                if (!inputs.length) {
                    return false;
                }

                $(inputs).each(function () {
                    $(this).removeClass('errors')
                        .closest('.form-group')
                        .find('.error-message')
                        .removeClass('hidden')
                        .addClass('hidden')
                });
            },

            showErrors = function (parent, errors) {


                if (!parent || !parent.length) {
                    return false;
                }

                var inputs = $(parent).find('.form-control');

                clearErrors(inputs);

                for (var i in errors) {
                    if (errors.hasOwnProperty(i)) {
                        $(parent)
                            .find('[name=' + i + ']')
                            .addClass('errors')
                            .closest('.form-group')
                            .find('.error-message')
                            .removeClass('hidden').text(errors[i]);
                    }
                }
            },

            collectMessage = function (field, m) {
                if (typeof field === "string" || typeof field === "number") {
                    return (field + "\n");
                }

                for (var i in field) {
                    if (field.hasOwnProperty(i)) {
                        m += collectMessage(field[i], "");
                    }
                }
                return m;
            },

            notify = function (status, message) {
                if (!notifyParams.hasOwnProperty(status)) {
                    status = 200;
                }

                var nMessage = collectMessage(message, "");

                var params = notifyParams[status];
                params.message = nMessage;

                $.notify(params,
                    {
                        element: 'body',
                        type: params['type'] || 'info',
                        allow_dismiss: true,
                        newest_on_top: true,
                        showProgressbar: false,
                        placement: {
                            from: params['from'] || 'top',
                            align: params['align'] || 'right'
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1033,
                        delay: 5000,
                        timer: 1000,
                        animate: {
                            enter: 'animated fadeIn',
                            exit: 'animated fadeOutDown'
                        }
                    });
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 1000);
                })
            },

            capitalizeFirstLetter = function (string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            };

        return {
            showErrors: showErrors,
            notify: notify,
            clearErrors: clearErrors,
            capitalizeFirstLetter: capitalizeFirstLetter
        }
    }()
);